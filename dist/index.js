'use strict';

var _koa = require('koa');

var _koa2 = _interopRequireDefault(_koa);

var _apolloServerKoa = require('apollo-server-koa');

var _prismaClient = require('../generated/prisma-client');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var resolvers = {
	Query: {
		publishedPosts: function publishedPosts(root, args, context) {
			return context.prisma.posts({ where: { published: true } });
		},
		post: function post(root, args, context) {
			return context.prisma.post({ id: args.postId });
		},
		postsByUser: function postsByUser(root, args, context) {
			return context.prisma.user({
				id: args.userId
			}).posts();
		}
	},
	Mutation: {
		createDraft: function createDraft(root, args, context) {
			return context.prisma.createPost({
				title: args.title,
				author: {
					connect: { id: args.userId }
				}
			});
		},
		publish: function publish(root, args, context) {
			return context.prisma.updatePost({
				where: { id: args.postId },
				data: { published: true }
			});
		},
		createUser: function createUser(root, args, context) {
			return context.prisma.createUser({ name: args.name });
		}
	},
	User: {
		posts: function posts(root, args, context) {
			return context.prisma.user({
				id: root.id
			}).posts();
		}
	},
	Post: {
		author: function author(root, args, context) {
			return context.prisma.post({
				id: root.id
			}).author();
		}
	}
};

// const server = new ApolloServer({ typeDefs: './schema.graphql', resolvers, context: { prisma } });
var app = new _koa2.default();
// server.applyMiddleware({ app });

app.listen({ port: 4000 }, function () {
	return console.log('\uD83D\uDE80 Server ready at http://localhost:4000');
});