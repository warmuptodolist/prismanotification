const { prisma } = require('../generated/prisma-client');
const { GraphQLServer } = require('graphql-yoga');
const admin = require('firebase-admin');
const serviceAccount = require('../serviceAccountKey');
const { NOTIFICATION_TOPIC } = require('./config/Constant');
const _ = require('lodash');

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: 'https://reactnativenotification-79c37.firebaseio.com',
});

const resolvers = {
	Query: {
		publishedPosts(root, args, context) {
			return context.prisma.posts({ where: { published: true } });
		},
		post(root, args, context) {
			return context.prisma.post({ id: args.postId });
		},
		postsByUser(root, args, context) {
			return context.prisma
				.user({
					id: args.userId,
				})
				.posts();
		},
		listAllUser(root, args, context) {
			return context.prisma.users();
		},
		getUserByID(root, args, context) {
			let { userId } = args;
			return context.prisma.user({ id: userId });
		},
	},
	Mutation: {
		createDraft(root, args, context) {
			return context.prisma.createPost({
				title: args.title,
				author: {
					connect: { id: args.userId },
				},
			});
		},
		publish(root, args, context) {
			return context.prisma.updatePost({
				where: { id: args.postId },
				data: { published: true },
			});
		},
		createUser(root, args, context) {
			return context.prisma.createUser({ name: args.name });
		},
		subscribeNotification(root, args, context) {
			console.log('user subscribe here', args);
			let { token } = args;

			var notification = {
				notification: {
					title: 'testing',
					body: ' this is for testing purpose',
				},
				data: {
					score: '850',
					time: '2:45',
				},
				token,
			};
			// var topic = 'highScores';

			// // See documentation on defining a message payload.
			// var message = {
			// 	data: {
			// 		score: '850',
			// 		time: '2:45'
			// 	},
			// 	topic: topic
			// };

			context.admin
				.messaging()
				.send(notification)
				.then((response) => {
					// Response is a message ID string.
					console.log('Successfully sent message:', response);
				})
				.catch((error) => {
					console.log('Error sending message:', error);
				});
			return "you've successfully subscribed the notification";
		},
		async updateFCMToken(root, args, context) {
			let { token, id } = args;
			let { prisma } = context;
			console.log('this is token request ====>', token, id);
			try {
				let response = await prisma.updateUser({ data: { token: token, name: 'Lyheng' }, where: { id } });
				console.log('thiisi data response ====>', response);
				return response;
			} catch (e) {
				console.log('error  updateFCMToken', e);
			}
		},
		async deliveryItem(root, args, context) {
			let { userId, itemName } = args;
			let { prisma, admin } = context;
			let user = await prisma.user({ id: userId });
			if (user) {
				if (_.get(user, 'token', null)) {
					//send notification to client
					let { token } = user;
					let notification = {
						android: {
							priority: 'high',
							notification: {
								title: 'Joonaak Delivery service',
								body: `We are delivering ${itemName} to you`,
							},
							data: {
								score: '850',
								time: '2:45',
							},
						},
						token,
					};
					admin
						.messaging()
						.send(notification)
						.then((response) => {
							// Response is a message ID string.
							console.log('Successfully sent message:', response);
						})
						.catch((error) => {
							console.log('Error sending message:', error);
						});

					return 'you have successfully sent client a notication';
				} else {
					///cannot send notification to client
					return 'client has not yet subscribe notification, so that we require them to subscribe first!';
				}
			}
		},

		async giveRejectedReason(root, args, context) {
			let { prisma, admin } = context;
			let { reason } = args;

			console.log('reason of rejected =======>', reason);
			let notification = {
				android: {
					priority: 'high',
					notification: {
						title: 'Got Rejected',
						body: reason,
					},
					data: {
						score: '850',
						time: '2:45',
					},
				},
				topic: NOTIFICATION_TOPIC.REJECTED_DEVELIVERY,
			};
			admin
				.messaging()
				.send(notification)
				.then((response) => {
					// Response is a message ID string.
					console.log('Successfully sent message:', response);
				})
				.catch((error) => {
					console.log('Error sending message:', error);
				});

			return 'Send successful';
		},

		itemIsAccepted(roots, args, context) {
			let notification = {
				android: {
					priority: 'high',
					notification: {
						title: 'Got accepted delivery',
						body: 'Please Start deliver Item',
					},
					data: {
						score: '850',
						time: '2:45',
					},
				},
				topic: NOTIFICATION_TOPIC.ACCEPTED_DELIVERY,
			};

			admin
				.messaging()
				.send(notification)
				.then((response) => {
					console.log('Successfully sent message:', response);
				})
				.catch((error) => {
					console.log('got error ====>', error);
				});
			return 'Item is accepted delivery';
		},
	},
	User: {
		posts(root, args, context) {
			return context.prisma
				.user({
					id: root.id,
				})
				.posts();
		},
	},
	Post: {
		author(root, args, context) {
			return context.prisma
				.post({
					id: root.id,
				})
				.author();
		},
	},
};

const server = new GraphQLServer({
	typeDefs: './schema.graphql',
	resolvers,
	context: {
		prisma,
		admin,
	},
});
server.start(() => console.log('Server is running on http://localhost:4000'));
