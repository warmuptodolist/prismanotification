const FIREBASE_URL = 'https://reactnativenotification-79c37.firebaseio.com';
const NOTIFICATION_TOPIC = {
	REJECTED_DEVELIVERY: 'rejected_delivery',
	ACCEPTED_DELIVERY: 'accepted_delivery',
};
module.exports = {
	FIREBASE_URL,
	NOTIFICATION_TOPIC,
};
